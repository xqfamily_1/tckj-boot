package com.tckj.boot;

import com.tckj.boot.modules.domain.sys.menu.model.Menu;
import com.tckj.boot.modules.domain.sys.menu.model.MenuRepository;
import com.tckj.boot.modules.domain.sys.role.model.Role;
import com.tckj.boot.modules.domain.sys.role.model.RoleRepository;
import com.tckj.boot.modules.domain.sys.user.application.UserService;
import com.tckj.boot.modules.domain.sys.user.model.User;
import com.tckj.boot.modules.domain.sys.user.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * @author 田小强
 * @date 2019年05月10日 上午9:29
 */
@Component
public class BaseInitalizer {

    private static UserService userService;
    private static UserRepository userRepository;
    private static RoleRepository roleRepository;
    private static MenuRepository menuRepository;

    @Autowired
    public BaseInitalizer(UserService userService,UserRepository userRepository,RoleRepository roleRepository,MenuRepository menuRepository) {
        BaseInitalizer.userService=userService;
        BaseInitalizer.userRepository=userRepository;
        BaseInitalizer.roleRepository=roleRepository;
        BaseInitalizer.menuRepository=menuRepository;
        initMenu();
        initRole();
        initUser();
    }



    private List<Menu> initMenu(){
        List<Menu> menus = menuRepository.findAll();
        if(menus.size() <= 0){
            Menu root = new Menu("根", null, "根！", "", 1L, null, Menu.Type.ROOT, "skin-green");
/*
* 綠色 skin-green  深藍skin-dark-blue   粉紅skin-pink   紫色skin-purple   藍色skin-blue
* */
            Menu gn = new Menu("备用菜单", "shield-security", "备用菜单,根据自己的项目修改名称！", "", 2L, root, Menu.Type.SYSTEM, "skin-dark-blue");
            Menu sysMng = new Menu("系统管理", "shield-security", "系统监控！", "", 5L, root, Menu.Type.SYSTEM, "skin-green");
            Menu sysJk = new Menu("系统监控", "shield-security", "系统监控！", "", 8L, root, Menu.Type.SYSTEM, "skin-cloud");

            Menu index = new Menu("首页", "zmdi-home", "首页呀这是！", "home", 1L, sysMng, Menu.Type.MENU, "skin-green");
            Menu system = new Menu("系统管理", "zmdi-accounts-list", "系统管理！", "", 3L, sysMng, Menu.Type.MENU, "skin-green");
            Menu systemLog = new Menu("系统日志", "zmdi-account", "系统日志管理！", "page/sys/log/table.html", 1L, system, Menu.Type.MENU, "skin-green");
            Menu codeManger = new Menu("代码生成", "zmdi-account", "代码生成管理！", "page/gennerator/index.html", 2L, system, Menu.Type.MENU, "skin-green");

            Menu userManage = new Menu("角色用户管理", "zmdi-accounts", "角色用户管理！", "", 5L, sysMng, Menu.Type.MENU, "skin-green");
            Menu user = new Menu("用户管理", "zmdi-account", "用户管理！", "page/sys/user/table.html", 1L, userManage, Menu.Type.MENU, "skin-green");
            Menu role = new Menu("角色管理", "zmdi-account", "角色管理！", "page/sys/role/table.html", 3L, userManage, Menu.Type.MENU, "skin-green");
            Menu menuManage = new Menu("权限资源管理", "zmdi-lock-outline", "权限资源管理！", "home", 8L, sysMng, Menu.Type.MENU, "skin-green");
            Menu menu = new Menu("权限管理", "zmdi-home", "权限管理！", "page/sys/menu/menu.html", 1L, menuManage, Menu.Type.MENU, "skin-green");

            Menu test = new Menu("备用测试", "zmdi-home", "备用测试！", "page/project/index.html", 5L, gn, Menu.Type.MENU, "skin-green");

            menus.add(root);
            menus.add(gn);
            menus.add(sysMng);
            menus.add(sysJk);
            menus.add(index);
            menus.add(system);
            menus.add(systemLog);
            menus.add(codeManger);
            menus.add(userManage);
            menus.add(user);
            menus.add(role);
            menus.add(menuManage);
            menus.add(menu);
            menus.add(test);
        }
        menuRepository.saveAll(menus);
        System.err.println("菜单权限资源管理："+menus.size());
        return menus;
    }

    private void initRole(){
        List<Role> roleList = roleRepository.findAll();
        if(roleList.size() <= 0){
            Role admin = new Role("ADMIN", "管理员", "ADMIN");
            admin.setMenuSet(new HashSet<>(initMenu()));
            roleList.add(admin);
            roleList.add(new Role("CUSTOM","客户","CUSTOM"));
            roleRepository.saveAll(roleList);
        }
        System.err.println("角色初始化："+roleList.size());
    }


    private void initUser(){
        Optional<User> user = userRepository.findByUsername("admin");
//        Set<Role> roles = (Set<Role>) roleRepository.findAll();
        User admin = new User("admin", "123456");
        admin.setRoleSet(new HashSet<>(roleRepository.findAll()));
        User result = user.orElseGet(()->userService.save(admin));
        System.err.println("用户初始化："+result.getUsername());
    }

}
