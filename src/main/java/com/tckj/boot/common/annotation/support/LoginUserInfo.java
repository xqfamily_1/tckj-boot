package com.tckj.boot.common.annotation.support;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName LoginUserInfo
 * @Description
 * @Author icodebug
 * @Date 19-12-11 下午2:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserInfo implements Serializable {
    Long userId;
    String userName;
    String openid;
}
