package com.tckj.boot.generator.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.template.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tckj.boot.generator.domain.GenConfig;
import com.tckj.boot.generator.domain.vo.ColumnInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.util.*;

/**
 * 代码生成
 * @author tckj
 * @date 2019-01-02
 */
@Slf4j
public class GenUtil {

    private static final String TIMESTAMP = "Timestamp";
    private static final String DATE = "Date";
    private static final String BIGDECIMAL = "BigDecimal";
    private static final String PK = "PRI";
    private static final String EXTRA = "auto_increment";
    private static final String FORMAT ="yyyy-MM-dd HH:mm:ss";

    /**
     * 获取后端代码模板名称
     * @return
     */
    public static List<String> getAdminTemplateNames() {
        List<String> templateNames = new ArrayList<>();
        templateNames.add("Entity");
        templateNames.add("Dto");
//        templateNames.add("Mapper");
        templateNames.add("Repository");
        templateNames.add("Service");
//        templateNames.add("ServiceImpl");
        templateNames.add("QueryCriteria");
        templateNames.add("Controller");
        templateNames.add("WxController");
        return templateNames;
    }

    /**
     * 获取前端代码模板名称
     * @return
     */
    public static List<String> getFrontTemplateNames() {
        List<String> templateNames = new ArrayList<>();
//        templateNames.add("api");
        templateNames.add("index");
//        templateNames.add("header");
        templateNames.add("create");
        templateNames.add("edit");
//        templateNames.add("eForm");
        return templateNames;
    }

    /**
     * 生成代码
     * @param columnInfos 表元数据
     * @param genConfig 生成代码的参数配置，如包路径，作者
     */
    public static void generatorCode(List<ColumnInfo> columnInfos, GenConfig genConfig, String tableName) throws IOException {
        Map<String,Object> map = new HashMap();
        map.put("package",genConfig.getPack());
        map.put("moduleName",genConfig.getModuleName());
        map.put("author",genConfig.getAuthor());
        map.put("date", DateFormatUtils.format(new Date(),FORMAT));
        map.put("tableName",tableName);
        String className = StringUtils.toCapitalizeCamelCase(tableName);
        String changeClassName = StringUtils.toCamelCase(tableName);

        // 判断是否去除表前缀
        if (StringUtils.isNotEmpty(genConfig.getPrefix())) {
            className = StringUtils.toCapitalizeCamelCase(StrUtil.removePrefix(tableName,genConfig.getPrefix()));
            changeClassName = StringUtils.toCamelCase(StrUtil.removePrefix(tableName,genConfig.getPrefix()));
        }
        String domainPackagePath = "modules"+File.separator+"domain" + File.separator +"obj"+File.separator+className.toLowerCase();
        map.put("className", className);
        map.put("upperCaseClassName", className.toUpperCase());
        map.put("changeClassName", changeClassName);
        map.put("hasTimestamp",false);
        map.put("hasBigDecimal",false);
        map.put("hasQuery",false);
        map.put("auto",false);
        map.put("domainPackagePath",domainPackagePath.replace(File.separator,"."));

        List<Map<String,Object>> columns = new ArrayList<>();
        List<Map<String,Object>> queryColumns = new ArrayList<>();
        List<Map<String,Object>> enums = new ArrayList<>();
        for (ColumnInfo column : columnInfos) {
            Map<String,Object> listMap = new HashMap();
            listMap.put("columnComment",column.getColumnComment());
            listMap.put("columnKey",column.getColumnKey());

            String colType = ColUtil.cloToJava(column.getColumnType().toString());
            String changeColumnName = StringUtils.toCamelCase(column.getColumnName().toString());
            String capitalColumnName = StringUtils.toCapitalizeCamelCase(column.getColumnName().toString());
            if(PK.equals(column.getColumnKey())){
                map.put("pkColumnType",colType);
                map.put("pkChangeColName",changeColumnName);
                map.put("pkCapitalColName",capitalColumnName);
            }
            if(TIMESTAMP.equals(colType)){
                map.put("hasTimestamp",true);
            }
            if(DATE.equals(colType)){
                map.put("hasDate",true);
            }
            if(BIGDECIMAL.equals(colType)){
                map.put("hasBigDecimal",true);
            }
            if(EXTRA.equals(column.getExtra())){
                map.put("auto",true);
            }
            listMap.put("columnType",colType);
            listMap.put("columnName",column.getColumnName());
            listMap.put("isNullable",column.getIsNullable());
            listMap.put("columnShow",column.getColumnShow());
            listMap.put("changeColumnName",changeColumnName);
            listMap.put("capitalColumnName",capitalColumnName);
            listMap.put("hasEnum",false);//为了判断每个字段是不是枚举
            listMap.put("columnControl",column.getColumnControl());
            //判断数据库注释字段是否有扩展内容
            commentExtra(enums, column, listMap, colType, changeColumnName);


            if(!StringUtils.isBlank(column.getColumnQuery())){
                listMap.put("columnQuery",column.getColumnQuery());
                map.put("hasQuery",true);
                queryColumns.add(listMap);
            }
            columns.add(listMap);
        }
        map.put("enums",enums);
        map.put("columns",columns);
        map.put("queryColumns",queryColumns);
        TemplateEngine engine = TemplateUtil.createEngine(new TemplateConfig("template", TemplateConfig.ResourceMode.CLASSPATH));

        System.out.println("GenUtil Map :"+ JSON.toJSONString(map));
        // 生成后端代码
        List<String> templates = getAdminTemplateNames();
        for (String templateName : templates) {
            Template template = engine.getTemplate("generator/admin/"+templateName+".ftl");
            String filePath = getAdminFilePath(templateName,genConfig,className,domainPackagePath);

            File file = new File(filePath);

            // 如果非覆盖生成
            if(!genConfig.getCover()){
                if(FileUtil.exist(file)){
                    continue;
                }
            }
            // 生成代码
            genFile(file, template, map);
        }

        // 生成前端代码
        templates = getFrontTemplateNames();
        for (String templateName : templates) {
            Template template = engine.getTemplate("generator/front/"+templateName+".ftl");
            String filePath = getFrontFilePath(templateName,genConfig,map.get("changeClassName").toString().toLowerCase());
            System.out.println("前端：filePath："+filePath);
            File file = new File(filePath);

            // 如果非覆盖生成
            if(!genConfig.getCover()){
                if(FileUtil.exist(file)){
                    continue;
                }
            }
            // 生成代码
            genFile(file, template, map);
        }
    }

    //判断数据库注释字段是否有扩展内容
    private static void commentExtra(List<Map<String, Object>> enums, ColumnInfo column, Map<String, Object> listMap, String colType, String changeColumnName) {
        //是否枚举  默认值 枚举数据
        if (column.getColumnCommentExtra()!=null){
            String ce = column.getColumnCommentExtra().toString();
//            System.out.println(ce);
            JSONObject ceObj = JSONObject.parseObject(ce);
            listMap.put("defaultVal",ceObj.containsValue("default")?ceObj.getString("default"):null);
//todo 默认值这一块需要重写
            if (ceObj.containsKey("enum")){

                Map<String,Object> enumMap = new HashMap<>();


                enumMap.put("enumClassName",StringUtils.toCapitalizeCamelCase(changeColumnName+"Enum"));
                enumMap.put("changeColumnName",changeColumnName);
                enumMap.put("hasBoolean",true);

                if (!colType.equals("Boolean")){
                    enumMap.put("hasBoolean",false);
                    listMap.put("columnType",StringUtils.toCapitalizeCamelCase(changeColumnName+"Enum"));
                    listMap.put("hasEnum",true);//为了判断每个字段是不是枚举
                }

                List<Map<String,Object>> enumVals = new ArrayList<>();
                ceObj.getJSONArray("enum").forEach(e->{
                    Map<String,String> emk = (Map<String,String>) e;
                    emk.keySet().forEach(key->{
                        Map<String,Object> em = new HashMap<>();
                        em.put("lab",emk.get(key));
                        em.put("val",colType.equals("Boolean")?key:key.toUpperCase());
                        enumVals.add(em);
                    });
                });
                enumMap.put("enumVals",enumVals);
                enums.add(enumMap);

                listMap.put("defaultVal",ceObj.containsValue("default")?ceObj.getString("default").toUpperCase():null);
            }
        }
    }

    public static String getPackagePath(GenConfig genConfig){
        String ProjectPath = System.getProperty("user.dir") + File.separator;
        String packagePath = ProjectPath + "src" +File.separator+ "main" + File.separator + "java" + File.separator;
        if (!ObjectUtils.isEmpty(genConfig.getPack())) {
            packagePath += genConfig.getPack().replace(".", File.separator) + File.separator;
        }
        return packagePath;
    }

    /**
     * 定义后端文件路径以及名称
     */
    public static String getAdminFilePath(String templateName, GenConfig genConfig, String className,String domainPackagePath) {
        String packagePath = getPackagePath(genConfig);
        domainPackagePath = packagePath + domainPackagePath+File.separator;
        if ("Entity".equals(templateName)) {
            return domainPackagePath+ className + ".java";
        }

        if ("Repository".equals(templateName)) {
            return domainPackagePath + className + "Repository.java";
        }

        if ("Service".equals(templateName)) {
            return domainPackagePath + className + "Service.java";
        }

        if ("Dto".equals(templateName)) {
            return domainPackagePath + "dto" + File.separator + className + "DTO.java";
        }

        if ("QueryCriteria".equals(templateName)) {
            return domainPackagePath + "dto" + File.separator + className + "QueryCriteria.java";
        }

        if ("Controller".equals(templateName)) {
            return packagePath +"modules"+File.separator+ "web" + File.separator+ "admin"+File.separator + className.toLowerCase()+File.separator + className + "Controller.java";
        }

        if ("WxController".equals(templateName)) {
            return packagePath +"modules"+File.separator+ "web" + File.separator+ "wx"+File.separator + className.toLowerCase()+File.separator + "Wx"+className + "Controller.java";
        }

        if ("Mapper".equals(templateName)) {
            return packagePath + "service" + File.separator + "mapper" + File.separator + className + "Mapper.java";
        }



        return null;
    }

    /**
     * 定义前端文件路径以及名称
     */
    public static String getFrontFilePath(String templateName, GenConfig genConfig, String changeClassName) {
        String ProjectPath = System.getProperty("user.dir") + File.separator;
        String path = ProjectPath + "src" +File.separator+ "main" + File.separator + "resources" + File.separator+genConfig.getPath()+File.separator+changeClassName;
//        if (!ObjectUtils.isEmpty(genConfig.getPack())) {
//            path += genConfig.getPack().replace(".", File.separator) + File.separator;
//        }
//        String path = genConfig.getPath();

//        if ("api".equals(templateName)) {
//            return genConfig.getApiPath() + File.separator + apiName + ".js";
//        }

        if ("index".equals(templateName)) {
            return path  + File.separator + "index.html";
        }

        if ("header".equals(templateName)) {
            return path    + File.separator + "header.vue";
        }
        if ("create".equals(templateName)) {
            return path  + File.separator + "create.vue";
        }

        if ("edit".equals(templateName)) {
            return path + File.separator + "edit.vue";
        }

        if ("eForm".equals(templateName)) {
            return path + File.separator + "form.vue";
        }
        return null;
    }

    public static void genFile(File file,Template template,Map<String,Object> map) throws IOException {
        // 生成目标文件
        Writer writer = null;
        try {
            FileUtil.touch(file);
            writer = new FileWriter(file);
            template.render(map, writer);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            writer.close();
        }
    }
}
