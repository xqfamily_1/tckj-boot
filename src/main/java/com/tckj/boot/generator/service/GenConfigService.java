package com.tckj.boot.generator.service;

import com.tckj.boot.generator.domain.GenConfig;
import com.tckj.boot.generator.domain.GenConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author tckj
 * @date 2019-01-14
 */
@Service
public class GenConfigService {

    @Autowired
    private GenConfigRepository genConfigRepository;

    public GenConfig find() {
        Optional<GenConfig> genConfig = genConfigRepository.findById(1L);
        if(genConfig.isPresent()){
            return genConfig.get();
        } else {
            return new GenConfig();
        }
    }

    public GenConfig update(GenConfig genConfig) {
        genConfig.setId(1L);
        return genConfigRepository.save(genConfig);
    }
}
