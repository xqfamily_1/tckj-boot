package com.tckj.boot.generator.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tckj
 * @date 2019-01-14
 */
public interface GenConfigRepository extends JpaRepository<GenConfig,Long> {
}
