package com.tckj.boot.modules.web.admin;

import com.tckj.boot.modules.domain.sys.menu.application.MenuTreeService;
import com.tckj.boot.modules.web.admin.vo.MenuTreeVO;
import com.tckj.boot.modules.domain.sys.user.application.UserService;
import com.tckj.boot.modules.domain.sys.user.model.User;
import com.tckj.boot.modules.domain.sys.user.model.UserRepository;
import com.tckj.boot.modules.domain.sys.user.model.UserSpecification;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

/**
 * 用户控制器
 *
 * @author yangrd
 * @date 2019/1/9
 **/
@AllArgsConstructor

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserRepository repository;

    private UserService userService;

    private MenuTreeService menuTreeService;

    private PasswordEncoder passwordEncoder;

    @PostMapping
    public User add(@RequestBody User user) {
        user.setPassword("123456");
        return userService.save(user);
    }

    @DeleteMapping
    @Transactional(rollbackFor = Exception.class)
    public void delete(@RequestBody List<Long> ids) {
        repository.deleteInBatch(repository.findAllById(ids));
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") User user, @RequestBody User self) {
        user.setRoleSet(self.getRoleSet());
        user.setStatus(self.getStatus());
        user.setDetails(self.getDetails());
        repository.saveAndFlush(user);
    }

    @PutMapping("/{id}/password")
    public void changePw(@PathVariable("id") User user, @RequestBody String password) {
        user.setPassword(passwordEncoder.encode(password));
        repository.saveAndFlush(user);
    }

    @GetMapping("{id}")
    public User get(@PathVariable("id") User user) {
        return user;
    }

    @GetMapping
    public Page<User> findAll(User user, Pageable pageable) {
        return repository.findAll(UserSpecification.toSpec(user), pageable);
    }

    @GetMapping("/current")
    public String getCurrentUsername(Principal principal) {
        return ((User) ((UsernamePasswordAuthenticationToken) principal).getPrincipal()).getUsername();
    }

    @GetMapping("/current/menu")
    public Collection<MenuTreeVO> listCurrentUserMenu() {
        return menuTreeService.listCurrentUserSystem();
    }

}
