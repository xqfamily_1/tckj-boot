package com.tckj.boot.modules.web.wx.news2;

import com.tckj.boot.modules.domain.obj.news2.News2;
import com.tckj.boot.modules.domain.obj.news2.News2Service;
import com.tckj.boot.modules.domain.obj.news2.dto.News2QueryCriteria;
import com.tckj.boot.common.utils.ResponseUtil;
import com.tckj.boot.common.annotation.LoginUser;
import com.tckj.boot.common.annotation.support.LoginUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
* @author icodebug
* @date 2019-12-18 16:58:25
*/
@RestController("wxNews2Controller")
@RequestMapping("/wx/news2")
public class WxNews2Controller {

    @Autowired
    private News2Service service;

    @GetMapping()
    public ResponseEntity getNews2s(@LoginUser LoginUserInfo loginUserInfo,News2QueryCriteria criteria, Pageable pageable){
        if (loginUserInfo == null) {return ResponseEntity.ok(ResponseUtil.unlogin());}
        return ResponseEntity.ok(ResponseUtil.ok(service.queryAll(criteria,pageable)));
    }

    @PostMapping()
    public ResponseEntity create(@LoginUser LoginUserInfo loginUserInfo,@RequestBody News2 resources){
        if (loginUserInfo == null) {return ResponseEntity.ok(ResponseUtil.unlogin());}
        return new ResponseEntity(ResponseUtil.ok(service.create(resources)),HttpStatus.CREATED);
    }

}