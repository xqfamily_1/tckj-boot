package com.tckj.boot.modules.web.admin.news2;

import com.tckj.boot.modules.domain.obj.news2.News2;
import com.tckj.boot.modules.domain.obj.news2.News2Service;
import com.tckj.boot.modules.domain.obj.news2.dto.News2QueryCriteria;
import com.tckj.boot.common.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
* @author icodebug
* @date 2019-12-18 16:58:25
*/
@RestController("adminNews2Controller")
@RequestMapping("/admin/news2")
public class News2Controller {

    @Autowired
    private News2Service service;

    @GetMapping()
    public ResponseEntity getNews2s(News2QueryCriteria criteria, Pageable pageable){
        return ResponseEntity.ok(ResponseUtil.ok(service.queryAll(criteria,pageable)));
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody News2 resources){
        return new ResponseEntity(ResponseUtil.ok(service.create(resources)),HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity update(@RequestBody News2 resources){
        service.create(resources);
        return ResponseEntity.ok("");
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        service.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}