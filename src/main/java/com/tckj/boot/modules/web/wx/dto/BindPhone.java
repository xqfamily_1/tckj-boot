package com.tckj.boot.modules.web.wx.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName BindPhone
 * @Description
 * @Author icodebug
 * @Date 19-6-17 上午10:21
 */
@Getter
@Setter
public class BindPhone {

    private String encryptedData;
    private String iv;

}
