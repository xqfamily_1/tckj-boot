package com.tckj.boot.modules.web.admin.news;

import com.tckj.boot.modules.domain.obj.news.News;
import com.tckj.boot.modules.domain.obj.news.NewsService;
import com.tckj.boot.modules.domain.obj.news.dto.NewsQueryCriteria;
import com.tckj.boot.common.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
* @author icodebug
* @date 2019-12-12 11:37:22
*/
@RestController("adminNewsController")
@RequestMapping("/admin/news")
public class NewsController {

    @Autowired
    private NewsService service;

    @GetMapping()
    public ResponseEntity getNewss(NewsQueryCriteria criteria, Pageable pageable){
        return ResponseEntity.ok(ResponseUtil.ok(service.queryAll(criteria,pageable)));
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody News resources){
        return new ResponseEntity(ResponseUtil.ok(service.create(resources)),HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity update(@RequestBody News resources){
        service.create(resources);
        return ResponseEntity.ok("");
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        service.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}