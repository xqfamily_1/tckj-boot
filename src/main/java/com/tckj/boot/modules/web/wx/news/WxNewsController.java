package com.tckj.boot.modules.web.wx.news;

import com.tckj.boot.modules.domain.obj.news.News;
import com.tckj.boot.modules.domain.obj.news.NewsService;
import com.tckj.boot.modules.domain.obj.news.dto.NewsQueryCriteria;
import com.tckj.boot.common.utils.ResponseUtil;
import com.tckj.boot.common.annotation.LoginUser;
import com.tckj.boot.common.annotation.support.LoginUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
* @author icodebug
* @date 2019-12-12 11:37:22
*/
@RestController("wxNewsController")
@RequestMapping("/wx/news")
public class WxNewsController {

    @Autowired
    private NewsService service;

    @GetMapping()
    public ResponseEntity getNewss(@LoginUser LoginUserInfo loginUserInfo,NewsQueryCriteria criteria, Pageable pageable){
        if (loginUserInfo == null) {return ResponseEntity.ok(ResponseUtil.unlogin());}
        return ResponseEntity.ok(ResponseUtil.ok(service.queryAll(criteria,pageable)));
    }

    @PostMapping()
    public ResponseEntity create(@LoginUser LoginUserInfo loginUserInfo,@RequestBody News resources){
        if (loginUserInfo == null) {return ResponseEntity.ok(ResponseUtil.unlogin());}
        return new ResponseEntity(ResponseUtil.ok(service.create(resources)),HttpStatus.CREATED);
    }

}