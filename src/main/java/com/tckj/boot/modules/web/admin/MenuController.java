package com.tckj.boot.modules.web.admin;

import com.alibaba.fastjson.JSON;
import com.tckj.boot.common.utils.BeanUtils;
import com.tckj.boot.modules.domain.sys.menu.application.MenuTreeService;
import com.tckj.boot.modules.web.admin.vo.MenuTreeVO;
import com.tckj.boot.modules.domain.sys.menu.model.Menu;
import com.tckj.boot.modules.domain.sys.menu.model.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

/**
 * 菜单控制器
 * @author yangrd
 * @date 2019/1/9
 **/

@AllArgsConstructor

@RestController
@RequestMapping("/api/menus")
public class MenuController {

    private MenuRepository repository;

    private MenuTreeService menuTreeService;

    @PostMapping
    public Menu add(@RequestBody Menu menu){
        return repository.save(menu);
    }

    @DeleteMapping
    @Transactional(rollbackFor=Exception.class)
    public void delete(@RequestBody List<Long> ids){
        repository.deleteInBatch(repository.findAllById(ids));
    }

    @PutMapping("{id}")
    public void update(@PathVariable("id") Menu old, @RequestBody Menu self){
        BeanUtils.copyNotNullProperties(self,old);
        repository.saveAndFlush(old);
    }

    @GetMapping
    public Collection<MenuTreeVO> listSystem() {
        Collection<MenuTreeVO> menuTreeVOS = menuTreeService.listSystem();
        System.out.println(JSON.toJSONString(menuTreeVOS));
        return menuTreeService.listSystem();
    }

    @GetMapping("/{id}")
    public Menu get(@PathVariable("id") Menu menu){
        return menu;
    }

    @GetMapping("/{pid}/child")
    public List<Menu> findChildByParent(@PathVariable("pid") Menu parent){
        return repository.findByParentOrderBySortAsc(parent);
    }
}
