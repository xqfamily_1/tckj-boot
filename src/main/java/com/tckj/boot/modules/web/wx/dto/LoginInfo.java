package com.tckj.boot.modules.web.wx.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginInfo {
    private String code;
    private UserInfo userInfo;
    private String username;
    private String password;
}
