package com.tckj.boot.modules.web.admin;

import com.tckj.boot.generator.exception.BadRequestException;
import com.tckj.boot.modules.domain.sys.dict.DictDetail;
import com.tckj.boot.modules.domain.sys.dict.DictDetailService;
import com.tckj.boot.modules.domain.sys.dict.dto.DictDetailQueryCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author icodebug
 * @date 2019-07-08
 */
@RestController
@RequestMapping("/admin/dictdetail")
public class DictDetailController {

    @Autowired
    private DictDetailService dictDetailService;

    private static final String ENTITY_NAME = "dictDetail";

    @GetMapping
    public ResponseEntity getDictDetails(DictDetailQueryCriteria criteria,
                                         @PageableDefault(value = 10, sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable){
        return new ResponseEntity(dictDetailService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@Validated @RequestBody DictDetail resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return new ResponseEntity(dictDetailService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody DictDetail resources){
        dictDetailService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        dictDetailService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}