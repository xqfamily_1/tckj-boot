package com.tckj.boot.modules.web.wx;

import com.alibaba.fastjson.JSON;
import com.tckj.boot.common.annotation.LoginUser;
import com.tckj.boot.common.annotation.support.LoginUserInfo;
import com.tckj.boot.common.utils.ResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 用户服务
 */
@RestController
@RequestMapping("/wx/user")
@Validated
public class WxUserController {
    private final Log logger = LogFactory.getLog(WxUserController.class);


    /**
     * 用户个人页面数据
     * <p>
     * 目前是用户订单统计信息
     *
     * @param loginUserInfo 已登录的用户信息
     * @return 用户个人页面数据
     */
    @GetMapping()
    public Object list(@LoginUser LoginUserInfo loginUserInfo) {
        if (loginUserInfo == null) {
            return ResponseUtil.unlogin();
        }
        System.out.println(JSON.toJSONString(loginUserInfo));

        return ResponseUtil.ok(loginUserInfo);
    }

}