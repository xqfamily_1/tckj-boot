package com.tckj.boot.modules.domain.obj.news2;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
* @author icodebug
* @date 2019-12-18 16:58:25
*/
public interface News2Repository extends JpaRepository<News2, Long>, JpaSpecificationExecutor {
}