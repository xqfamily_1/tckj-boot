package com.tckj.boot.modules.domain.sys.storage.application.dto;

import com.tckj.boot.common.annotation.Query;
import lombok.Data;

/**
 * @ClassName StorageQueryDto
 * @Description
 * @Author icodebug
 * @Date 19-6-18 下午3:33
 */
@Data
public class StorageQueryDto {

    @Query(type = Query.Type.INNER_LIKE)
    private String key;

    @Query(type = Query.Type.INNER_LIKE)
    private String name;

}
