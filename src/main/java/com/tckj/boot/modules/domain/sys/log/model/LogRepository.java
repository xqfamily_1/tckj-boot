package com.tckj.boot.modules.domain.sys.log.model;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yangrd
 * @date 2019/1/11
 **/
public interface LogRepository extends JpaRepository<Log, Long> {

}
