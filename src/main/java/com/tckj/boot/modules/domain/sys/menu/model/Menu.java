package com.tckj.boot.modules.domain.sys.menu.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tckj.boot.common.spring.data.AbstractEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author yangrd
 * @date 2019/1/9
 **/

@Getter
@Setter

@Entity
@Table(name = "sys_menu")
public class Menu extends AbstractEntity<Menu> implements GrantedAuthority {

    /**
     * 名称
     */
    private String name;

    /**
     *　图标
     */
    private String icon;

    /**
     *　描述
     */
    private String description;

    /**
     *　路径
     */
    private String url;

    /**
     *　排序字段
     */
    private Long sort;

    /**
     *　父级菜单
     */
    @ManyToOne
    private Menu parent;

    /**
     * 菜单类别对应的深度
     */
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @Enumerated(EnumType.STRING)
    private Type depth;

    /**
     * 皮肤 查看skins.css对应的样式
     */
    private String skin;

    /**
     * 认证标识
     */
    private String authority;

    public Menu() {
    }

    public Menu(String name, String icon, String description, String url, Long sort, Menu parent, Type depth, String skin) {
        this.name = name;
        this.icon = icon;
        this.description = description;
        this.url = url;
        this.sort = sort;
        this.parent = parent;
        this.depth = depth;
        this.skin = skin;
    }

    public enum Type {
        /**
         * 根
         */
        ROOT,
        /**
         * 系统
         */
        SYSTEM,
        /**
         * 目录
         */
        DIRECTORY,
        /**
         * 菜单
         */
        MENU
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Menu menu = (Menu) o;
        return Objects.equals(name, menu.name) &&
                Objects.equals(icon, menu.icon) &&
                Objects.equals(getId(), menu.getId()) &&
                Objects.equals(description, menu.description) &&
                Objects.equals(url, menu.url) &&
                Objects.equals(sort, menu.sort) &&
                depth == menu.depth &&
                Objects.equals(skin, menu.skin) &&
                Objects.equals(authority, menu.authority);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), name,getId(), icon, description, url, sort, depth, skin, authority);
    }
}
