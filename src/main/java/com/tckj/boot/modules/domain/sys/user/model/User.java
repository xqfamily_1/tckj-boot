package com.tckj.boot.modules.domain.sys.user.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tckj.boot.common.spring.data.AbstractEntity;
import com.tckj.boot.modules.domain.sys.role.model.Role;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author yangrd
 * @date 2019/1/9
 **/

@Getter
@Setter
@Entity
@Table(name = "sys_user")
public class User extends AbstractEntity<User> implements UserDetails {

    /**
     * 用户名
     */
    @Column(unique = true, updatable = false)
    private String username;

    /**
     * 密码
     */
    @Column(nullable = false)
    private String password;

    /**
     * 状态
     */
    private UserStatus status;

    /**
     * 最后一次登录时间
     */
    private Date lastLoginTime;

    /**
     * 最后一次登录ip
     */
    private String lastLoginIp;

    /**
     * 角色
     */
    @ManyToMany(cascade = { CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private Set<Role> roleSet;

    /**
     * 明细
     */
    @Embedded
    private UserDetailsInfo details;

    private String weixinOpenid;

    private String sessionKey;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateDate;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    {
        status = UserStatus.NORMAL;
        details = new UserDetailsInfo();
    }

    public enum UserStatus {
        /**
         * 正常
         */
        NORMAL,
        /**
         * 异常
         */
        ABNORMAL,
        /**
         * 冻结
         */
        FROZEN
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(this.roleSet);
        grantedAuthorities.addAll(this.roleSet.stream().map(Role::getMenuSet).flatMap(Set::stream).collect(Collectors.toList()));
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !UserStatus.FROZEN.equals(this.status);
    }

    public String getSessionKey() {
        return sessionKey;
    }
}
