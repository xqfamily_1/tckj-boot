package com.tckj.boot.modules.domain.obj.category.application.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @ClassName Category
 * @Description 分类
 * @Author icodebug
 * @Date 19-06-19 上午11:23
 */
@Data
public class CategoryDto{
    private Long id;
    private String name;
    /** 图标路径 */
    private String ico;
    /** 描述 */
    private String description;
    /** 排序 */
    private String sort;
    /** 状态 false正常 true废除 */
    private Boolean disable;
    /** 备注 */
    private String remark;
    private List<CategoryDto> children = new ArrayList<>();
    private Boolean child;
    private Boolean _disableExpand;
    private Boolean _expanded=false;

    public Boolean get_disableExpand() {
        return !this.child;
    }
}
