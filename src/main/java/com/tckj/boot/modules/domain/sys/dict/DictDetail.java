package com.tckj.boot.modules.domain.sys.dict;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tckj.boot.common.spring.data.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author icodebug
 * @date 2019-07-08
 */
@Entity
@Data
@Table(name="dict_detail")
public class DictDetail extends AbstractEntity<Dict> {
    /**
     * 字典标签
     */
    @Column(name = "label",nullable = false)
    private String label;

    /**
     * 字典值
     */
    @Column(name = "value",nullable = false)
    private String value;

    /**
     * 排序
     */
    @Column(name = "sort")
    private String sort = "999";

    /**
     * 字典id
     */
    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "dict_id")
    private Dict dict;

}