package com.tckj.boot.modules.domain.sys.menu.application;

import com.tckj.boot.common.utils.mapper.BeanMapper;
import com.tckj.boot.modules.domain.sys.menu.model.Menu;
import com.tckj.boot.modules.domain.sys.menu.model.MenuRepository;
import com.tckj.boot.modules.web.admin.vo.MenuTreeVO;
import com.tckj.boot.modules.domain.sys.user.model.User;
import com.tckj.boot.modules.domain.sys.user.model.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author yangrd
 * @date 2019/1/11
 **/
@Service
@AllArgsConstructor
public class MenuTreeService {

    private MenuRepository menuRepository;

    private UserRepository userRepository;

    /**
     * 获取菜单
     */
    public Collection<MenuTreeVO> listSystem() {
        return getMenuTree().getChildren();
    }

    /**
     *
     需要用户登陆成功才能使用
     */
    public Collection<MenuTreeVO> listCurrentUserSystem() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Set<Menu> menuSet = userRepository.findByUsername(user.getUsername()).map(user1->user1.getRoleSet().stream().flatMap(role -> role.getMenuSet().stream()).collect(Collectors.toSet())).orElseThrow(RuntimeException::new);
        MenuTreeVO menuTree = getMenuTree(menuSet);
        return menuTree==null?null:menuTree.getChildren();
    }

    /**
     * 获取菜单树
     */
    private MenuTreeVO getMenuTree() {
        return getMenuTree(menuRepository.findAll());
    }

    private MenuTreeVO getMenuTree(Collection<Menu> menuCollection) {
        Map<Long, MenuTreeVO> menuTreeMap = menuCollection.stream().collect(Collectors.toMap(Menu::getId, o -> BeanMapper.map(o, MenuTreeVO.class)));
        final Long[] rootId = {1L};
        menuTreeMap.forEach((k, v) -> {
            Long parentId = v.getParentId();
            if (v.getName().equals("根")){
                rootId[0] =v.getId();
            }
            if (parentId != -1) {
                menuTreeMap.get(parentId).addChildren(v);
            }
        });
        return menuTreeMap.get(rootId[0]);
    }
}
