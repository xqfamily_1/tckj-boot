package com.tckj.boot.modules.domain.sys.storage.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FileStorageRepository extends JpaRepository<FileStorage,Long>,JpaSpecificationExecutor {

    FileStorage findByKey(String key);
    Integer deleteByKey(String key);

}
