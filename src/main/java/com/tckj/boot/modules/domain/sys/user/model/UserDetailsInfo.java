package com.tckj.boot.modules.domain.sys.user.model;

import lombok.Data;

/**
 * @author yangrd
 * @date 2019/1/15
 **/
@Data
public class UserDetailsInfo {

    /**
     * 性别  0未知,1男性,2女性
     */
    private Byte gender;

    /**
     *　昵称
     */
    private String nickname;

    /**
     *　头像
     */
    private String avatar;

    /**
     *　邮箱
     */
    private String email;

    /**
     *　电话
     */
    private String phone;


}
