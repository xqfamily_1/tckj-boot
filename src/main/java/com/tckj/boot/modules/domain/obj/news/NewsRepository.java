package com.tckj.boot.modules.domain.obj.news;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
* @author icodebug
* @date 2019-12-12 11:37:22
*/
public interface NewsRepository extends JpaRepository<News, Long>, JpaSpecificationExecutor {
}