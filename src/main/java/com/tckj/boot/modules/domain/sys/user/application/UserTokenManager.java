package com.tckj.boot.modules.domain.sys.user.application;

import com.tckj.boot.common.annotation.support.LoginUserInfo;
import com.tckj.boot.common.utils.JwtHelper;

/**
 * 维护用户token
 */
public class UserTokenManager {

	public static String generateToken(Long id,String userName,String openid) {
        JwtHelper jwtHelper = new JwtHelper();
        return jwtHelper.createToken(id,userName,openid);
    }
    public static LoginUserInfo getUserId(String token) {
    	JwtHelper jwtHelper = new JwtHelper();
        LoginUserInfo u = jwtHelper.verifyTokenAndGetUserId(token);
        return u;
    }
}
