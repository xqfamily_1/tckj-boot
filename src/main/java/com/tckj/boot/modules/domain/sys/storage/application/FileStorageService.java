package com.tckj.boot.modules.domain.sys.storage.application;

import com.tckj.boot.common.utils.BeanUtils;
import com.tckj.boot.modules.domain.sys.storage.model.FileStorage;
import com.tckj.boot.modules.domain.sys.storage.model.FileStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName FileStorageService
 * @Description
 * @Author icodebug
 * @Date 19-6-18 上午11:27
 */
@Service
public class FileStorageService {

    @Autowired
    private FileStorageRepository repository;

    public FileStorage add(String name, Integer size, String type, String key, String url){
        return repository.saveAndFlush(new FileStorage(name,size,type,key,url));
    }

    public FileStorage findByKey(String key) {
        return repository.findByKey(key);
    }

    public FileStorage findById(Long id){
        return repository.findById(id).get();
    }

    public Integer deleteByKey(String key){
        return repository.deleteByKey(key);
    }

}
