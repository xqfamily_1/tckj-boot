package com.tckj.boot.modules.domain.obj.category.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName Category
 * @Description 分类
 * @Author icodebug
 * @Date 19-06-19 上午11:23
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "tc_category")
public class Category extends AbstractPersistable<Long> {
    private String name;
    /** 图标路径 */
    private String ico;
    /** 描述 */
    private String description;
    /** 排序 */
    private String sort;
    /** 状态 false正常 true废除 */
    private Boolean disable = false;
    /** 备注 */
    private String remark;

    /**
     *　父级菜单
     */
    @JsonIgnore
    @ManyToOne
    private Category parent;

/*    @JsonIgnore
    @OneToMany(mappedBy = "parent",cascade={CascadeType.PERSIST,CascadeType.REMOVE})
    private Set<Category> children = new HashSet<>();*/

    @Formula("(select count(*) from tc_category t where t.parent_id = id)")
    private Boolean child;

    public Category(String name, String ico, String description, String sort, String remark,Category parent) {
        this.name = name;
        this.ico = ico;
        this.description = description;
        this.sort = sort;
        this.remark = remark;
        this.parent=parent;
    }
}
