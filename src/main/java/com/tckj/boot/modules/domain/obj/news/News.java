package com.tckj.boot.modules.domain.obj.news;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.*;
import com.tckj.boot.common.spring.data.AbstractEntity;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;

/**
* @author icodebug
* @date 2019-12-12 11:37:22
*/
@Entity
@Data
@EqualsAndHashCode
@Table(name="tc_news")
public class News extends AbstractEntity<News> {
    /**
     * 标题
     */
    @Column(name = "title")
    private String title;
    /**
     * 创建用户id
     */
    @Column(name = "user_id")
    private Long userId;
    /**
     * 创建人昵称
     */
    @Column(name = "user_nick_name")
    private String userNickName;
    /**
     * 手机号
     */
    @Column(name = "mobile")
    private String mobile;
    /**
     * 邮箱
     */
    @Column(name = "email")
    private String email;
    /**
     * 详细内容
     */
    @Column(name = "detail")
    private String detail;
    /**
     * 地址
     */
    @Column(name = "address")
    private String address;
    /**
     * 来源
     */
    @Column(name = "source")
    private String source;

    @Enumerated(EnumType.STRING)
    private TestEnum typeTest = TestEnum.PHONE;

    {

    }

    private enum  TestEnum {
        WX,/*微信*/
        PHONE,/*手机号*/

    }
}