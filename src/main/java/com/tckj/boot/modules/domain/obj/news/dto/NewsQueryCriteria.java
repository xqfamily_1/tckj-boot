package com.tckj.boot.modules.domain.obj.news.dto;

import lombok.Data;
import com.tckj.boot.common.annotation.Query;

/**
* @author icodebug
* @date 2019-12-12 11:37:22
*/
@Data
public class NewsQueryCriteria{

    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String title;


    // 精确
    @Query
    private String userNickName;


    // 日期区间
    @Query(type = Query.Type.BETWEEN)
    private String createdDate;


    // 日期时间区间
    @Query(type = Query.Type.BETWEEN)
    private String lastModifiedDate;

}