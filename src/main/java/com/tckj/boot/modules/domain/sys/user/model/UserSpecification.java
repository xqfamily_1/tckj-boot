package com.tckj.boot.modules.domain.sys.user.model;

import com.github.wenhao.jpa.Specifications;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * UserSpecification
 *
 * @author yangrd
 * @date 2019/05/31
 */
public class UserSpecification {

    public static Specification<User> toSpec(final User user) {
        return Specifications.<User>and()
                .like(StringUtils.isNotEmpty(user.getUsername()), "username", user.getUsername())
                .build();
    }
}
