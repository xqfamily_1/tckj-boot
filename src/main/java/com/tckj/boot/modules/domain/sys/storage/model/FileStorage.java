package com.tckj.boot.modules.domain.sys.storage.model;

import com.tckj.boot.common.spring.data.AbstractEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName FileStorageService
 * @Description
 * @Author icodebug
 * @Date 19-6-18 上午10:50
 */
@Data
@Entity
@Table(name = "tc_file_storage")
public class FileStorage extends AbstractEntity {

    private String name;
    @Column(name = "file_size",length = 10)
    private Integer size;
    @Column(name = "file_type")
    private String type;
    @Column(name = "file_key")
    private String key;
    private String url;
    private Boolean deleted = false;

    public FileStorage() {
    }

    public FileStorage(String name, Integer size, String type, String key, String url) {
        this.name = name;
        this.size = size;
        this.type = type;
        this.key = key;
        this.url = url;
    }
}
