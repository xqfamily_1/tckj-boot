package com.tckj.boot.modules.domain.obj.category.model;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRespository extends JpaRepository<Category,Long> {
    List<Category> findByParent(Category parent, Sort sort);
    List<Category> findByParentIsNull(Sort sort);
}
