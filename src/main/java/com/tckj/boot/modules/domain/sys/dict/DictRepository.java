package com.tckj.boot.modules.domain.sys.dict;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
* @author icodebug
* @date 2019-07-08
*/
public interface DictRepository extends JpaRepository<Dict, Long>, JpaSpecificationExecutor {
}