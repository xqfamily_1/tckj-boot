package com.tckj.boot.modules.domain.sys.role.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yangrd
 * @date 2019/1/9
 **/
public interface RoleRepository extends JpaRepository<Role,Long> {

    /**
     * 根据名称模糊查询
     * @param name name
     * @param pageable pageable
     * @return Page<Role>
     */
    Page<Role> findAllByNameContains(String name, Pageable pageable);

    List<Role> findByName(String name);
}
