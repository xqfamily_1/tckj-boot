package com.tckj.boot.modules.domain.sys.dict.dto;

import com.tckj.boot.common.annotation.Query;
import lombok.Data;

/**
* @author Zheng Jie
* @date 2019-04-10
*/
@Data
public class DictDetailQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    private String label;

    @Query(propName = "name",joinName = "dict")
    private String dictName;
}