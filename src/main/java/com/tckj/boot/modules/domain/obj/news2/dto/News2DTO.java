package com.tckj.boot.modules.domain.obj.news2.dto;

import com.tckj.boot.modules.domain.obj.news2.News2;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;


/**
* @author icodebug
* @date 2019-12-18 16:58:25
*/
@Data
public class News2DTO implements Serializable {

    /**
     * 主键id
     */
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 创建用户id
     */
    private Long userId;

    /**
     * 创建人昵称
     */
    private String userNickName;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 详细内容
     */
    private String detail;

    /**
     * 地址
     */
    private String address;

    /**
     * 来源
     */
    private String source;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;

    /**
     * 最后修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedDate;

    /**
     * 联系方式
     */
    private News2.Typetestenum typeTest;

    /**
     * 状态
     */
    private Boolean status;
}