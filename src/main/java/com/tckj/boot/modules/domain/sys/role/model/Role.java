package com.tckj.boot.modules.domain.sys.role.model;

import com.tckj.boot.common.spring.data.AbstractEntity;
import com.tckj.boot.modules.domain.sys.menu.model.Menu;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * @author yangrd
 * @date 2019/1/9
 **/

@Getter
@Setter

@Entity
@Table(name = "sys_role")
public class Role extends AbstractEntity<Role> implements GrantedAuthority {

    private final static String ROLE_PREFIX = "ROLE_";

    /**
     * 角色名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 角色权限标示
     */
    private String authority;

    /**
     * 拥有的菜单
     */
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Menu> menuSet;

    @Override
    public String getAuthority() {
        return Optional.ofNullable(authority).map(authority -> authority.contains(ROLE_PREFIX) ? authority : ROLE_PREFIX + authority).orElse(null);
    }

    public Role() {
    }

    public Role(String name, String description, String authority) {
        this.name = name;
        this.description = description;
        this.authority = authority;
    }
    public void addMenu(Menu menu){
        if (this.menuSet==null)
            this.menuSet=new HashSet<>();
        this.menuSet.add(menu);
    }
}
