package com.tckj.boot.modules.domain.sys.log.application;

import com.tckj.boot.modules.domain.sys.log.model.Log;
import com.tckj.boot.modules.domain.sys.log.model.LogRepository;
import com.tckj.boot.common.utils.IpUtils;
import lombok.AllArgsConstructor;
import nl.bitwalker.useragentutils.UserAgent;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author yangrd
 * @date 2019/1/11
 **/
@AllArgsConstructor

@Service
public class LogService {

    private LogRepository logRepository;

    public void saveLog(HttpServletRequest request, Object handler, Exception ex, Long executeUseMillisecond) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String currentUsername = authentication.getName();
            Log log = new Log();
            log.setTitle("");
            log.setReqUri(request.getRequestURI());
            log.setReqMethod(request.getMethod());
            log.setController(handlerMethod.getBeanType().getName());
            log.setMethodName(handlerMethod.getMethod().getName());
            log.setUsername(currentUsername);
            log.setAddressIp(IpUtils.getIpAddr(request));
            //获取浏览器信息
            String ua = request.getHeader("User-Agent");
            //转成UserAgent对象
            UserAgent userAgent = UserAgent.parseUserAgentString(ua);
            log.setBrowserName(userAgent.getBrowser().getName());
            log.setDriverName(userAgent.getOperatingSystem().getName());
            if (Objects.nonNull(ex)) {
                String message = ex.getMessage() != null && ex.getMessage().length() > 1000 ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                String class1 = ex.getClass() != null && ex.getClass().toString().length() > 1000 ? ex.getClass().toString().substring(0, 500) : ex.getClass().toString();
                log.setExceptionInfo(class1 + message);
            }
            log.setExecuteUseMillisecond(executeUseMillisecond);
            asyncSave(log);
        }
    }

    @Async
    protected void asyncSave(Log log) {
        logRepository.save(log);
    }
}
