package com.tckj.boot.modules.domain.obj.news2;

import com.tckj.boot.modules.domain.obj.news2.dto.News2DTO;
import com.tckj.boot.modules.domain.obj.news2.dto.News2QueryCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.tckj.boot.common.utils.QueryHelp;
import com.tckj.boot.common.utils.BeanUtils;

/**
* @author icodebug
* @date 2019-12-18 16:58:25
*/
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class News2Service{

    @Autowired
    private News2Repository repository;

    public Page<News2> queryAll(News2QueryCriteria criteria, Pageable pageable){
        return repository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
    }

    public List<News2> queryAll(News2QueryCriteria criteria){
        return repository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder));
    }

    public News2 findById(Long id) {
        Optional<News2> news2 = repository.findById(id);
        return news2.get();
    }

    @Transactional(rollbackFor = Exception.class)
    public News2 create(News2 resources) {
        if (resources.getId()==null){
            return repository.saveAndFlush(resources);
        }else{
            News2 target = findById(resources.getId());
            BeanUtils.copyNotNullProperties(resources,target);
            return repository.saveAndFlush(target);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        repository.deleteById(id);
    }
}