package com.tckj.boot.modules.domain.obj.news2.dto;

import com.tckj.boot.modules.domain.obj.news2.News2;
import lombok.Data;
import com.tckj.boot.common.annotation.Query;

/**
* @author icodebug
* @date 2019-12-18 16:58:25
*/
@Data
public class News2QueryCriteria{
    // 精确
    @Query
    private String title;
    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String mobile;
    // 日期区间
    @Query(type = Query.Type.BETWEEN)
    private String createdDate;
    // 精确
    @Query
    private News2.Typetestenum typeTest;
    // 精确
    @Query
    private Boolean status;
}