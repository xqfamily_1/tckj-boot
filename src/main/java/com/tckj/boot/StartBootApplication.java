package com.tckj.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author yangrd
 * @date 2019/1/8
 **/
@SpringBootApplication
@EnableAsync
@EnableJpaAuditing
public class StartBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(StartBootApplication.class, args);
    }
}
