<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/plugins/iview/css/iview.css">

</head>
<body>

<div id="app" style="margin: 5px">

    <create ref="createtem" @save="created"></create>
    <edit ref="edittem" @edit="edited"></edit>
    <div style="background: #f5f7f9; padding: 10px 30px;">

     <#if hasQuery>
         <#if queryColumns??>
             <#list queryColumns as column>
                 <#if column.columnControl = '2'>
         ${column.columnComment}:<Date-Picker v-model="${column.changeColumnName}" @on-change="dateChange($event,'${column.changeColumnName}')" format="yyyy-MM-dd" type="daterange" placeholder="选择时间" style="width: 200px;margin-right: 15px;"></Date-Picker>
                 <#elseif column.columnControl = '3'>
         ${column.columnComment}:<Date-Picker v-model="${column.changeColumnName}" @on-change="dateChange($event,'${column.changeColumnName}')" format="yyyy-MM-dd HH:mm:ss" type="datetimerange" placeholder="选择时间" style="width: 280px;margin-right: 15px;"></Date-Picker>
                 <#elseif column.columnControl = '4'>
         ${column.columnComment}:
                 <i-select clearable label-in-value v-model="params.${column.changeColumnName}" style="width:200px;margin-right: 15px;">
                     <i-option v-for="item in ${column.changeColumnName}Data" :value="item.val" :key="item.val" :lable="item.lab">{{ item.lab }}</i-option >
                 </i-select>
                 <#else>
         ${column.columnComment}:<input v-model="params.${column.changeColumnName}"  autocomplete="off" spellcheck="false" type="text" placeholder="${column.columnComment}" class="ivu-input ivu-input-default" style="width: 160px;margin-right: 15px;">
                 </#if>
             </#list>
         </#if>
     </#if>

        <i-button type="warning"  @click="search"> 搜索 </i-button>
        <i-button type="success"  @click="addShow"> +添加 </i-button>
    </div>


    <div style="min-height: 600px;">
        <i-table border :columns="columns" :data="tableData" :loading="loading"></i-table>
    </div>
    <div style="text-align: center;margin: 15px;">
        <Page :total="totalElements" :page-size="params.size" @on-change="changePage" show-total></Page>
    </div>


</div>
<script src="/plugins/vue/vue.min.js"></script>
<script src="/plugins/iview/js/iview.min.js"></script>
<script src="/plugins/vue/httpVueLoader.js"></script>
<script src="/plugins/vue/axios.min.js"></script>
<script src="/app/js/i-axios.js"></script>
<script src="/app/js/utils.js"></script>

<script type="text/javascript">

    var vm = new Vue({
        el: '#app',
        components: {
            'create': httpVueLoader('create.vue'),
            'edit': httpVueLoader('edit.vue'),
        },
        data() {
            return {
                loading:false,
                columns: [
                    {
                        type: 'index',
                        width:50,
                        align: 'center'
                    },

      <#if columns??>
          <#list columns as column>
              <#if column.columnShow>
                  {
                      title: '${column.columnComment}',
                      key: '${column.changeColumnName}',
                      align: 'center',
                  <#if column.hasEnum || column.columnType='Boolean'>
                      render: (h, params) => {
                          let obj = this.${column.changeColumnName}Data.find(i=>i.val==params.row.${column.changeColumnName})
                          if (obj){
                              return h('div', obj.lab);
                          }else{
                              return h('div', '');
                          }
                      }
                  </#if>
                  },
              </#if>
          </#list>
      </#if>
                    {
                        title: '操作',
                        key: 'action',
                        align: 'center',
                        render: (h, params) => {
                            let btns = [];

                            btns.push(
                                h('Button', {
                                    props: {
                                        type: 'info',
                                        size: 'small'
                                    },
                                    style: {
                                        marginRight: '8px'
                                    },
                                    on: {
                                        click: () => {
                                            this.edit(params.index)
                                        }
                                    }
                                }, '修改')
                            )
                            btns.push(
                                h('Button', {
                                    props: {
                                        type: 'info',
                                        size: 'small'
                                    },
                                    style: {
                                        marginRight: '8px'
                                    },
                                    on: {
                                        click: () => {
                                            this.del(params.index)
                                        }
                                    }
                                }, '删除')
                            )
                            return h('div', btns);
                        }
                    }
                ],
                tableData:[],
                params:{
        <#if hasQuery>
            <#if queryColumns??>
                <#list queryColumns as column>
                    ${column.changeColumnName}:undefined,
                </#list>
            </#if>
        </#if>
                    page:0,
                    size:15,
                },
                totalElements:0,
    <#if enums??>
        <#list enums as em>
                ${em.changeColumnName}Data:[
            <#if em.enumVals??>
                <#list em.enumVals as lv>
                    {
                        <#if lv.val=="false" || lv.val=="true">
                        val:${lv.val},
                        <#else>
                        val:'${lv.val}',
                        </#if>
                        lab:'${lv.lab}',
                    },
                </#list>
            </#if>
                ],
        </#list>
    </#if>
        <#if hasQuery>
             <#if queryColumns??>
                 <#list queryColumns as column>
                    <#if column.columnControl= '2' || column.columnControl = '3'>
                    ${column.changeColumnName}:undefined,
                    </#if>
                 </#list>
             </#if>
        </#if>
                editIndex:undefined,
            }
        },
        mounted() {
            this.load();
        },
        methods:{
            load() {
                console.info("table load")
                let _this = this;
                _this.loading=true;
                iget('/admin/${changeClassName}',this.params,
                    successRes => {
                        _this.tableData=successRes.data.content
                        _this.totalElements=successRes.data.totalElements
                        _this.loading=false;
                    },failureRes => {
                        console.info("加载数据异常",failureRes)
                    }
                )
            },
            search() {
                this.changePage(0)
            },
            changePage: function (page) {
                this.params.page = page-1;
                this.load();
            },
            edit(index){
                this.editIndex=index;
                this.$refs.edittem.edit(Object.assign({}, this.tableData[index]));
            },
            edited(data){
                this.tableData.splice(this.editIndex,1,data);
            },
            dateChange(e,name){
                this.params[name]=e.toString().length<2?undefined:e.toString();
            },
            addShow(){
                this.$refs.createtem.show=true;
            },
            created(data){
                this.tableData.push(data);
            },
            del(index){
                let _this = this;
                let d = _this.tableData[index]
                idelete('/admin/${changeClassName}/'+d.id,null,
                    successRes => {
                        this.$Notice.error({
                            title: '删除成功'
                        });
                        _this.tableData.splice(index,1);
                    },failureRes => {
                        console.info("加载数据异常",failureRes)
                    }
                )
            },
        }
    });
</script>

</body>
</html>