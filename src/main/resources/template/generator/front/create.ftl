<template>
    <Modal
            v-model="show"
            title="添加"
            @on-ok="save"
            @on-cancel="close"
    >
        <i-form ref="formFilter" label-position="right" :label-width="100">
        <#if columns??>
            <#list columns as column>
                <#if column.changeColumnName != 'createdDate' && column.changeColumnName != 'lastModifiedDate' && column.changeColumnName != 'id'>
            <form-item label="${column.columnComment}：">
                    <#if column.columnControl = '2'>
                <Date-Picker v-model="${column.changeColumnName}" @on-change="dateChange($event,'${column.changeColumnName}')" format="yyyy-MM-dd" type="daterange" placeholder="选择时间" ></Date-Picker>
                    <#elseif column.columnControl = '3'>
                <Date-Picker v-model="${column.changeColumnName}" @on-change="dateChange($event,'${column.changeColumnName}')" format="yyyy-MM-dd HH:mm:ss" type="datetimerange" placeholder="选择时间" ></Date-Picker>
                    <#elseif column.columnControl = '4'>
                <i-select clearable label-in-value v-model="dto.${column.changeColumnName}">
                    <i-option v-for="item in ${column.changeColumnName}Data" :value="item.val" :key="item.val" :lable="item.lab">{{ item.lab }}</i-option >
                </i-select>
                    <#else>
                <i-input v-model="dto.${column.changeColumnName}"  autocomplete="off" spellcheck="false" type="text" placeholder="${column.columnComment}" />
                    </#if>
            </form-item>
                </#if>
            </#list>
        </#if>
        </i-form>
    </Modal>
</template>

<script>
    module.exports = {
        props:[],
        data() {
            return {
                show:false,
                dto:{
            <#if columns??>
                <#list columns as column>
                    <#if column.changeColumnName != 'createdDate' && column.changeColumnName != 'lastModifiedDate' && column.changeColumnName != 'id'>
                    ${column.changeColumnName}:undefined,
                    </#if>
                </#list>
            </#if>
                },
    <#if enums??>
        <#list enums as em>
                ${em.changeColumnName}Data:[
            <#if em.enumVals??>
                <#list em.enumVals as lv>
                    {
                        val:'${lv.val}',
                        lab:'${lv.lab}',
                    },
                </#list>
            </#if>
                ],
        </#list>
    </#if>
        <#if hasQuery>
            <#if queryColumns??>
                <#list queryColumns as column>
                    <#if column.columnControl= '2' || column.columnControl = '3'>
                ${column.changeColumnName}:undefined,
                    </#if>
                </#list>
            </#if>
        </#if>
                }
        },
        mounted() {

        },
        methods:{
            save() {
                let _this = this;
                _this.loading=true;
                ipost('/admin/${changeClassName}',
                    _this.dto,
                    successRes => {
                        this.$Notice.success({
                            title: '保存成功'
                        });
                        _this.show=false;
                        _this.dto=cleanParams(_this.dto);
                        this.$emit("save",successRes.data);
                    },failureRes => {
                        console.info("加载数据异常asdfg",failureRes)
                        this.$Notice.error({
                            title: '系统异常'
                        });
                    }
                )
            },
            dateChange(e,name){
                this.dto[name]=e.toString().length<2?undefined:e.toString();
            },
        }
    }
</script>
