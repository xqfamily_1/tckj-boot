package ${package}.modules.web.admin.${domainPackagePath};

import ${package}.${domainPackagePath}.${className};
import ${package}.${domainPackagePath}.${className}Service;
import ${package}.${domainPackagePath}.dto.${className}QueryCriteria;
import ${package}.common.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
* @author ${author}
* @date ${date}
*/
@RestController("admin${className}Controller")
@RequestMapping("/admin/${changeClassName}")
public class ${className}Controller {

    @Autowired
    private ${className}Service service;

    @GetMapping()
    public ResponseEntity get${className}s(${className}QueryCriteria criteria, Pageable pageable){
        return ResponseEntity.ok(ResponseUtil.ok(service.queryAll(criteria,pageable)));
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody ${className} resources){
        return new ResponseEntity(ResponseUtil.ok(service.create(resources)),HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity update(@RequestBody ${className} resources){
        service.create(resources);
        return ResponseEntity.ok("");
    }

    @DeleteMapping(value = "/{${pkChangeColName}}")
    public ResponseEntity delete(@PathVariable ${pkColumnType} ${pkChangeColName}){
        service.delete(${pkChangeColName});
        return new ResponseEntity(HttpStatus.OK);
    }
}