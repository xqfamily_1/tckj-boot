package ${package}.${domainPackagePath}.dto;

import ${package}.${domainPackagePath}.${className};
import lombok.Data;
<#if hasTimestamp>
import java.sql.Timestamp;
</#if>
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
<#if queryColumns??>
import ${package}.common.annotation.Query;
</#if>

/**
* @author ${author}
* @date ${date}
*/
@Data
public class ${className}QueryCriteria{
<#if queryColumns??>
    <#list queryColumns as column>
    <#if column.columnQuery = '1'>
    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private ${column.columnType} ${column.changeColumnName};
    </#if>
    <#if column.columnQuery = '2'>
    // 精确
    @Query
        <#if column.hasEnum>
    private ${className}.${column.columnType} ${column.changeColumnName};
        <#else >
    private ${column.columnType} ${column.changeColumnName};
        </#if>
    </#if>
    <#if column.columnQuery = '3'>
    // 日期区间
    @Query(type = Query.Type.BETWEEN)
    private String ${column.changeColumnName};
    </#if>
    </#list>
</#if>
}