package ${package}.${domainPackagePath};

import ${package}.${domainPackagePath}.dto.${className}DTO;
import ${package}.${domainPackagePath}.dto.${className}QueryCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ${package}.common.utils.QueryHelp;
import ${package}.common.utils.BeanUtils;

/**
* @author ${author}
* @date ${date}
*/
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ${className}Service{

    @Autowired
    private ${className}Repository repository;

    public Page<${className}> queryAll(${className}QueryCriteria criteria, Pageable pageable){
        return repository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
    }

    public List<${className}> queryAll(${className}QueryCriteria criteria){
        return repository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder));
    }

    public ${className} findById(${pkColumnType} ${pkChangeColName}) {
        Optional<${className}> ${changeClassName} = repository.findById(${pkChangeColName});
        return ${changeClassName}.get();
    }

    @Transactional(rollbackFor = Exception.class)
    public ${className} create(${className} resources) {
        if (resources.getId()==null){
            return repository.saveAndFlush(resources);
        }else{
            ${className} target = findById(resources.getId());
            BeanUtils.copyNotNullProperties(resources,target);
            return repository.saveAndFlush(target);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(${pkColumnType} ${pkChangeColName}) {
        repository.deleteById(${pkChangeColName});
    }
}