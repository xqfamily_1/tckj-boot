package ${package}.${domainPackagePath};

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.*;
import ${package}.common.spring.data.AbstractEntity;
<#if hasTimestamp>
import java.sql.Timestamp;
</#if>
<#if hasDate?? && hasDate>
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
</#if>
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
import java.io.Serializable;

/**
* @author ${author}
* @date ${date}
*/
@Entity
@Data
@EqualsAndHashCode
@Table(name="${tableName}")
public class ${className} extends AbstractEntity<${className}> {
<#if columns??>
    <#list columns as column>
    <#if column.columnName != 'id' && column.changeColumnName != 'createdDate' && column.changeColumnName != 'lastModifiedDate'>
    /**
     * ${column.columnComment}
     */
        <#if column.columnType = 'Date'>
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        </#if>
        <#if column.hasEnum>
    @Enumerated(EnumType.STRING)
        </#if>
    @Column(name = "${column.columnName}")
    private ${column.columnType} ${column.changeColumnName};
    </#if>
    </#list>
</#if>

<#if enums??>
    <#list enums as em>
    <#if !em.hasBoolean>
    public enum  ${em.enumClassName} {
        <#if em.enumVals??>
            <#list em.enumVals as lv>
                ${lv.val},/*${lv.lab}*/
            </#list>
        </#if>
    }
    </#if>
    </#list>
</#if>


}