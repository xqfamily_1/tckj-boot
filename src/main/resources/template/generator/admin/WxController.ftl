package ${package}.modules.web.wx.${changeClassName};

import ${package}.${domainPackagePath}.${className};
import ${package}.${domainPackagePath}.${className}Service;
import ${package}.${domainPackagePath}.dto.${className}QueryCriteria;
import ${package}.common.utils.ResponseUtil;
import ${package}.common.annotation.LoginUser;
import ${package}.common.annotation.support.LoginUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
* @author ${author}
* @date ${date}
*/
@RestController("wx${className}Controller")
@RequestMapping("/wx/${changeClassName}")
public class Wx${className}Controller {

    @Autowired
    private ${className}Service service;

    @GetMapping()
    public ResponseEntity get${className}s(@LoginUser LoginUserInfo loginUserInfo,${className}QueryCriteria criteria, Pageable pageable){
        if (loginUserInfo == null) {return ResponseEntity.ok(ResponseUtil.unlogin());}
        return ResponseEntity.ok(ResponseUtil.ok(service.queryAll(criteria,pageable)));
    }

    @PostMapping()
    public ResponseEntity create(@LoginUser LoginUserInfo loginUserInfo,@RequestBody ${className} resources){
        if (loginUserInfo == null) {return ResponseEntity.ok(ResponseUtil.unlogin());}
        return new ResponseEntity(ResponseUtil.ok(service.create(resources)),HttpStatus.CREATED);
    }

}