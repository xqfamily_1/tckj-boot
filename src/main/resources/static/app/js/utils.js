/**
 * 排序  orderBy(users, ['name', 'age'], ['asc', 'desc']); // [{name: 'barney', age: 36}, {name: 'fred', age: 48}, {name: 'fred', age: 40}]
 * @param arr
 * @param props
 * @param orders
 * @returns {*[]}
 */
var orderBy = (arr, props, orders) =>
    [...arr].sort((a, b) =>
        props.reduce((acc, prop, i) => {
            if (acc === 0) {
                const [p1, p2] = orders && orders[i] === 'desc' ? [b[prop], a[prop]] : [a[prop], b[prop]];
                acc = p1 > p2 ? 1 : p1 < p2 ? -1 : 0;
            }
            return acc;
        }, 0)
    );

/**
 * 处理空的参数
 * @param datas
 * @returns
 */
var cleanParams = function(datas){
    var v_data ={};
    for(var a in datas){
        if (datas[a] != null && datas[a] instanceof Array) {
            v_data[a]=[];
        }else {
            v_data[a] = null;
        }
    }
    return v_data;
}