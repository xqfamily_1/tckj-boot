package com.tckj.boot;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @ClassName MainTest
 * @Description
 * @Author icodebug
 * @Date 19-6-18 上午11:35
 */
public class MainTest {
    public static void main(String[] args) {
//        System.out.println(System.getProperty("user.dir") +"  "+ File.separator);
//        System.out.println(RandomStringUtils.randomAlphanumeric(20));
//        System.out.println(LocalDateTime.now().toString());

//        String str = "{\"aa\":\"bb\"}";
        String str = "{\"enum\":[{\"wx\":\"微信\"},{\"qq\":\"QQ\"},{\"phone\":\"手机号\"}]}";
//        System.out.println(str.replaceAll("'","\\\\'"));
        System.out.println(JSONObject.parseObject(str));

    }
}
