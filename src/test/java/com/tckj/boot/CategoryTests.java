package com.tckj.boot;

import com.alibaba.fastjson.JSON;
import com.tckj.boot.modules.domain.obj.category.model.Category;
import com.tckj.boot.modules.domain.obj.category.model.CategoryRespository;
import com.tckj.boot.modules.domain.sys.menu.model.Menu;
import com.tckj.boot.modules.domain.sys.menu.model.MenuRepository;
import com.tckj.boot.modules.domain.sys.user.model.User;
import com.tckj.boot.modules.domain.sys.user.model.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryTests {

    @Autowired
    private CategoryRespository respository;


    @Test
    public void initUser(){
        Category category = respository.findById(142L).get();
        respository.saveAndFlush(new Category("子分类142-2","aa.png","简介","2","备注",category));
//        System.out.printf(JSON.toJSONString(respository.findAll()));
    }



}

