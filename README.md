# tckj Boot  v1.0

>  去繁就简 重新出发

基于Spring Boot 集成一些常用的功能，你只需要基于它做些简单的修改即可。已经集成微信小程序接口自动生成。

### 演示环境：

网址: 

用户名/密码: admin/123456

 **注意事项：**  密码不要改 : 

### 功能列表：

* [x] 权限认证 
 
* [x] 权限管理
 
* [x] 用户管理 

* [x] 角色管理 

* [x] 日志管理

* [x] 代码生成

### 项目结构：
```
tckj-boot
├─java
│  ├─common 公共模块
│  │  ├─annotation 自定义注解
│  │  ├─spring spring相关的功能
│  │  └─utils 常用工具
│  │ 
│  ├─config 公共模块
│  │  ├─interceptor 拦截器配置
│  │  ├─security 认证相关配置
│  │  ├─storage 文件存储相关配置
│  │  └─wx 微信相关配置
│  │
│  ├─generator 代码生成核心模块
│  │
│  ├─infrastructure 外部资源对接模块
│  │  └─storage 文件存储调用
│  │ 
│  ├─modules 功能模块
│  │  └─sys 权限模块
│  │ 
│  └─StartBootApplication 项目启动类
│  
└─resources 
   ├─static 第三方库、插件等静态资源
   │ ├─app 项目中自己写的css js img 等资源文件
   │ ├─page 页面
   | │ ├─gennerator 代码生成操作页面
   | │ ├─project 项目功能页面
   | │ └─sys 系统功能页面
   | |
   │ └─plugins 第三方库、插件等静态资源
   │ 
   ├─template 模板文件存放
   │ └─generator 代码生成页面模板文件
   │ 
   └─application.yml  项目配置文件
```

### 代码片段：
```java
/**
 * 角色控制器
 *
 * @author yangrd
 * @date 2019/1/9
 **/
@RestController
@RequestMapping("/api/roles")
@AllArgsConstructor
public class RoleController {

    private RoleRepository repository;

    @PostMapping
    public Role add(@RequestBody Role role) {
        return repository.save(role);
    }

    @DeleteMapping
    @Transactional(rollbackFor = Exception.class)
    public void delete(@RequestBody List<Long> ids) {
        repository.deleteInBatch(repository.findAllById(ids));
    }

    @PutMapping("{id}")
    public void update(@PathVariable("id") Role old, @RequestBody Role self) {
        old.setName(self.getName());
        old.setAuthority(self.getAuthority());
        old.setMenuSet(self.getMenuSet());
        old.setDescription(self.getDescription());
        repository.saveAndFlush(old);
    }

    @GetMapping("{id}")
    public Role get(@PathVariable("id") Role role) {
        return role;
    }

    @GetMapping
    public Page<Role> findAll(@RequestParam(defaultValue = "") String name, Pageable pageable) {
        return repository.findAllByNameContains(name, pageable);
    }
}
```

### 代码自动创建数据库备注规则：
XX;{"enum":[{"val":"lab"}],"defalut":"xx"}
XX;{"enum":[{"true":"正常"},{"false","作废"}]}
XX;{"enum":[{"wx":"微信"},{"qq"："QQ"},{"phone":"手机号"}]}

自动生成代码枚举类型，前端页面select暂时只支持单选查询，后期增加多选查询


### 注意事项：

运行项目前导入[tckj-boot.sql](tckj-boot.sql)

### 技术栈(技术选型)：

**后端:**

核心框架 ：Spring Boot 2.1.1.RELEASE

安全框架：Apache security

视图框架：Spring MVC

持久层框架：Spring Data JPA

数据库连接池：HikariDataSource

日志管理：LogBack

JSON序列号框架: fastjson

插件: lombok 

**前端:**

主要使用的技术：

渐进式JavaScript 框架：VUE 2.2.0 ， iview

页面主体框架 ：zhengAdmin

### 效果图：

![用户管理](resources/20190118155259.png)

![菜单管理](resources/20190118154424.png)

![角色管理](resources/20190118154502.png)

![日志查看](resources/20190118154530.png)

![嵌入外部网址](resources/20190118154651.png)

### 生成新的项目

step1. 首先在配置好的工程根目录下执行mvn archetype:create-from-project，成功后如图所示：
step2. 进入到target/generated-sources/archetype目录，执行mvn install，此时工程模板已经被安装到maven本地仓库，可以通过执行如下命令按照提示构建一个新的maven工程
setp3. 生成项目
mvn archetype:generate -DarchetypeGroupId=com.tckj.boot -DarchetypeArtifactId=tckj-boot-archetype -DarchetypeVersion=1.0 -DgroupId=com.tckj.demo  -DartifactId=demo -Dversion=1.0  -Dpackage=com.tckj.demo

mvn archetype:generate -DarchetypeGroupId=com.tckj.boot -DarchetypeArtifactId=tckj-boot-archetype -DarchetypeVersion=1.0 -DgroupId=com.tckj.jyjzs  -DartifactId=jyjzs -Dversion=1.0  -Dpackage=com.tckj.jyjzs

mvn archetype:generate -DarchetypeGroupId=com.tckj.boot -DarchetypeArtifactId=tckj-boot-archetype -DarchetypeVersion=1.0 -DgroupId=com.sj.sjzxnew  -DartifactId=sjzxnew -Dversion=1.0  -Dpackage=com.sj.sjzxnew

mvn archetype:generate -DarchetypeGroupId=com.tckj.boot -DarchetypeArtifactId=tckj-boot-archetype -DarchetypeVersion=1.0 -DgroupId=com.tckj.parity  -DartifactId=parity -Dversion=1.0  -Dpackage=com.tckj.parity

### 扩展：

[zhengAdmin](https://github.com/shuzheng/zhengAdmin/blob/master/README.md)

[使用Vue](https://cn.vuejs.org/v2/guide/)

**[Spring Boot 学习资料](https://segmentfault.com/a/1190000008539153)**

ico
https://material.io/resources/icons/?icon=android&style=baseline

页面解决方案：https://github.com/FranckFreiburger/http-vue-loader
https://github.com/zhouxianjun/iview-tree-table/blob/master/example/Example.vue

eladmin


//需要自动增加菜单权限，需要优化boolean类型，需要增加对默认值的支持，需要增加图片显示操作


